#------------------------------------------------------------------------------+
#
#   Nathan A. Rooy
#   Simple Particle Swarm Optimization (PSO) with Python
#   July, 2016
#
#------------------------------------------------------------------------------+

#--- IMPORT DEPENDENCIES ------------------------------------------------------+

from __future__ import division
import random
import math
import numpy as np
import operator
import matplotlib.pyplot as plt

#--- COST FUNCTION ------------------------------------------------------------+

# function we are attempting to optimize (minimize)
def sphere(x):
    total=0
    for i in range(len(x)):
        total+=x[i]**2
    return total

def rastring(x):
    y = 10 * len(x) + sum(map(lambda i: i**2 - 10 * np.cos(2*np.pi*i), x))
    return y

def ackley(x):
    dim = len(x)
    term1 = -1. * 20 * np.exp(-1. * 0.2 * np.sqrt((1./dim) * sum(map(lambda i: i**2, x))))
    term2 = -1. * np.exp((1./dim) * (sum(map(lambda j: np.cos(2*np.pi * j), x))))        
    return term1 + term2 + 20 + np.exp(1)

#--- MAIN ---------------------------------------------------------------------+

class Particle:
    def __init__(self,x0):
        self.position_i=[]          # particle position
        self.velocity_i=[]          # particle velocity
        self.pos_best_i=[]          # best position individual
        self.fitness_best_i=-1      # best fitness individual
        self.fitness_i=-1           # fitness individual
        self.neighborhood = []      # neighborhood (2 neighbors)

        for i in range(0,num_dimensions):
            self.velocity_i.append(random.uniform(-1,1))
            self.position_i.append(x0[i])

    # evaluate current fitness
    def evaluate(self,costFunc):
        self.fitness_i=costFunc(self.position_i)

        # check to see if the current position is an individual best
        if self.fitness_i<self.fitness_best_i or self.fitness_best_i==-1:
            self.pos_best_i=self.position_i
            self.fitness_best_i=self.fitness_i
                    
    # update new particle velocity
    def update_velocity(self,pos_best_g,w,constriction):
        #w=0.5       # constant inertia weight (how much to weigh the previous velocity)
        c1=2.1        # cognitive constant
        c2=2.1        # social constant
        if constriction == "True":
            phi = c1+c2
            k = 2/abs(2-phi-math.sqrt(phi**2 - 4*phi))
        elif constriction == "False":
            k = 1
        
        for i in range(0,num_dimensions):
            r1=random.random()
            r2=random.random()
            
            vel_cognitive=c1*r1*(self.pos_best_i[i]-self.position_i[i])
            vel_social=c2*r2*(pos_best_g[i]-self.position_i[i])
            self.velocity_i[i]=k*(w*self.velocity_i[i]+vel_cognitive+vel_social)

    # update the particle position based off new velocity updates
    def update_position(self,bounds):
        for i in range(0,num_dimensions):
            self.position_i[i]=self.position_i[i]+self.velocity_i[i]
            
            # adjust maximum position if necessary
            if self.position_i[i]>bounds[i][1]:
                self.position_i[i]=bounds[i][1]

            # adjust minimum position if neseccary
            if self.position_i[i]<bounds[i][0]:
                self.position_i[i]=bounds[i][0]

    def neighborhood_find(self, swarm, num_neighbors):
        list_aux = []
        self.neighborhood = []
        for j in range(len(swarm)):
            if(self != swarm[j]):
                total = 0
                for k in range(len(bounds)):
                    total += abs(self.position_i[k] - swarm[j].position_i[k])
                list_aux.append([total,swarm[j]])
        list_aux.sort(key = operator.itemgetter(0))
        for i in range(num_neighbors):
            self.neighborhood.append(list_aux[i][1])
        
class PSO():

    def __init__(self,costFunc,bounds,inertia=0.5,num_particles=50,maxiter=100,mode="global",num_neighbors=2,constriction="False"):
        global num_dimensions
        num_dimensions=len(bounds)
        self.fitness_best_g=-1               # best fitness for group
        self.pos_best_g=[]                 # best position for group
        self.bests = []
        # establish the swarm
        swarm=[]   
        for i in range(0,num_particles):
            position = []
            for i in range(num_dimensions):
                x_i = random.uniform(bounds[i][0], bounds[i][1])
                position.append(x_i)
            swarm.append(Particle(position))

        # begin optimization loop
        i=0

        # if mode == 'local':
        #     while i<maxiter:
        #         for particle in swarm:
        #             particle.evaluate(costFunc)
        #             fitness_best_l = particle.fitness_best_i
        #             pos_best_l = particle.pos_best_i
        #             #print (i,fitness_best_g, pos_best_g)
        #             particle.neighborhood_find(swarm,num_neighbors)

        #             # cycle through particles in swarm and evaluate fitness
        #             for neighborhood in particle.neighborhood:
        #                 neighborhood.evaluate(costFunc)

        #             # determine if current particle is the best (globally)
        #             if neighborhood.fitness_i<fitness_best_l:
        #                 pos_best_l=list(neighborhood.position_i)
        #                 fitness_best_l=float(neighborhood.fitness_i)
        #             particle.update_velocity(pos_best_l,inertia,constriction)
        #             particle.update_position(bounds)

        #             if particle.fitness_i<self.fitness_best_g or self.fitness_best_g==-1:
        #                 self.pos_best_g=list(particle.position_i)
        #                 self.fitness_best_g=float(particle.fitness_i)
        #         i+=1
        #         self.bests.append(self.fitness_best_g)        
    #elif mode == 'global':
        while i<maxiter:
            #print (i,fitness_best_g)
            # cycle through particles in swarm and evaluate fitness
            for j in range(0,num_particles):
                swarm[j].evaluate(costFunc)

                # determine if current particle is the best (globally)
                if swarm[j].fitness_i<self.fitness_best_g or self.fitness_best_g==-1:
                    self.pos_best_g=list(swarm[j].position_i)
                    self.fitness_best_g=float(swarm[j].fitness_i)
            
            # cycle through swarm and update velocities and position
            for j in range(0,num_particles):
                swarm[j].update_velocity(self.pos_best_g,inertia,constriction)
                swarm[j].update_position(bounds)
            i+=1
            self.bests.append(self.fitness_best_g)

        # print final results
        #print ('FINAL:')
        #print (pos_best_g)
        #print (fitness_best_g)

if __name__ == "__PSO__":
    main()

#--- RUN ----------------------------------------------------------------------+

bounds=[(-5,5),(-5,5),(-5,5)]  # input bounds [(x1_min,x1_max),(x2_min,x2_max)...]

sphere_data = []
rastrigin_data = []
ackley_data = []
inertia = [0.5]
w = 1
constriction = ["False", "True"]
mode = ["global"]
for y in constriction:
    print(y)
    data1 = [[] for k in range(100)]
    data2 = [[] for k in range(100)]
    data3 = [[] for k in range(100)]
    for i in range(30):
        x = PSO(sphere,bounds,0.5, mode="global", constriction=y)
        best_individual = x.bests
        for j in range(100):
            data1[j].append(best_individual[j])
        
        x = PSO(rastring,bounds,0.5, mode="global", constriction=y)
        best_individual = x.bests
        for j in range(100):
            data2[j].append(best_individual[j])
        
        x = PSO(rastring,bounds,0.5, mode="global", constriction=y)
        best_individual = x.bests
        for j in range(100):
            data3[j].append(best_individual[j])
    sphere_data.append(data1)
    rastrigin_data.append(data2)
    ackley_data.append(data3)

# GRÁFICO
mean_sphere = []
error_sphere = []
i = 0
for item in sphere_data:
    mean_sphere.append([])
    error_sphere.append([])
    for data_set in item:
        mean_sphere[i].append(np.mean(data_set))
        error_sphere[i].append(np.std(data_set))
    i += 1

mean_rastrigin = []
error_rastrigin = []
i = 0
for item in rastrigin_data:
    mean_rastrigin.append([])
    error_rastrigin.append([])
    for data_set in item:
        mean_rastrigin[i].append(np.mean(data_set))
        error_rastrigin[i].append(np.std(data_set))
    i += 1

mean_ackley = []
error_ackley = []
i = 0
for item in ackley_data:
    mean_ackley.append([])
    error_ackley.append([])
    for data_set in item:
        mean_ackley[i].append(np.mean(data_set))
        error_ackley[i].append(np.std(data_set))
    i += 1

x = [i for i in range(1,101)]

plt.figure()

plt.subplot(3, 1, 1)
plt.errorbar(x, mean_sphere[0], yerr=error_sphere[0])
plt.errorbar(x, mean_sphere[1], yerr=error_sphere[1])
plt.title("Sphere")
plt.legend(['Sem fator de restrição', 'Com fator de restrição'], loc=2)
plt.xlabel("Iteração")
plt.ylabel("Fitness")

plt.subplot(3, 1, 2)
plt.errorbar(x, mean_rastrigin[0], yerr=error_rastrigin[0])
plt.errorbar(x, mean_rastrigin[1], yerr=error_rastrigin[1])
plt.title("Rastigin")
plt.legend(['Sem fator de restrição', 'Com fator de restrição'], loc=2)
plt.xlabel("Iteração")
plt.ylabel("Fitness")

plt.subplot(3, 1, 3)
plt.errorbar(x, mean_ackley[0], yerr=error_ackley[0])
plt.errorbar(x, mean_ackley[1], yerr=error_ackley[1])
plt.title("Ackley")
plt.legend(['Sem fator de restrição', 'Com fator de restrição'], loc=2)
plt.xlabel("Iteração")
plt.ylabel("Fitness")

plt.tight_layout()
plt.show()

#--- END ----------------------------------------------------------------------+
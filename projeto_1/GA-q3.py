import random
import numpy as np
import matplotlib.pyplot as plt

def log(population,populationFitness,current_ite):
  maxFitness_index = 0
  for i in range(1,len(populationFitness)):
    if populationFitness[i] > populationFitness[maxFitness_index]:
        maxFitness_index = i
  best_individual = []
  for x in population[maxFitness_index]:
    best_individual.append(x/100.0)
  #print('#', current_ite, ' Best fitness = ', -1*populationFitness[maxFitness_index], '  ', best_individual)
  return [best_individual, -1*populationFitness[maxFitness_index]]


def initialPopulation(populationSize, upperBound, lowerBound):
  population = []
  numberVar = len(upperBound)
  for i in range(populationSize):
    individual = []
    for v in range(numberVar):
      individual.append(random.randint(lowerBound[v]*100, upperBound[v]*100))
      #individual.append(0)
    population.append(individual)
  return population

def divideby100(individual):
  x = []
  for y in individual:
    x.append(y/100.0)
  return x

'''
FUNCTIONS
'''
def sphere(individual):
  x = divideby100(individual)
  y = sum(map(lambda var: var**2, x))
  return -1*y

def rastringin(individual, dimension):
  x = divideby100(individual)
  y = dimension * len(x) + sum(map(lambda var: var**2 - dimension * np.cos(2*np.pi*var), x))
  return -1*y

def ackley(individual):
  x = divideby100(individual)
  dim = len(x)
  term1 = -1. * 20 * np.exp(-1. * 0.2 * np.sqrt((1./dim) * sum(map(lambda i: i**2, x))))
  term2 = -1. * np.exp((1./dim) * (sum(map(lambda j: np.cos(2*np.pi * j), x))))       
  return -1*(term1 + term2 + 20 + np.exp(1))

def GoldsteinPrice(individual):
  x = divideby100(individual)
  a = 1+(x[0]+x[1]+1)**2*(19-14*x[0]+3*x[0]**2-14*x[1]+6*x[0]*x[1]+3*x[1]**2)
  b = 30+(2*x[0]-3*x[1])**2*(18-32*x[0]+12*x[0]**2+48*x[1]-36*x[0]*x[1]+27*x[1]**2)
  return -1*a*b

def avalFitness(population,fitnessFunction):
    populationFitness = []
    for individual in population:        
        if fitnessFunction == "Sphere":
          populationFitness.append(sphere(individual))
        elif fitnessFunction == "Rastrigin":
          populationFitness.append(rastringin(individual,3))
        elif fitnessFunction == "Ackley":
          populationFitness.append(ackley(individual))
        elif fitnessFunction == "GoldsteinPrice":
          populationFitness.append(GoldsteinPrice(individual))
    return populationFitness

# selecao de parentes - eletisa
def parents_selection(population, populationFitness, population_size):

    parents = []
    templist = []

    for i in range(len(population)):
    	templist.append([population[i], populationFitness[i]])

    lst = [item[0] for item in templist[0:population_size]]

    i = 0
    while i <= int(len(lst)/2):
        parents.append([lst[i], lst[i+1]])
        parents.append([lst[i+1], lst[i]])
        i += 2

    return parents

  # selecao da populacao - elitista
def population_selection(population, offspring, populationFitness, offspringFitness, population_size):
  templist=[]
  for i in range(len(population)):
    templist.append([population[i], populationFitness[i]])
  
  for i in range(len(offspring)):
    templist.append([offspring[i], offspringFitness[i]])
    
  templist = sorted(templist, key=lambda x: x[1], reverse=True)
  lst2 = [item[0] for item in templist[0:population_size]]
  return lst2 # retorna metade da populacao merged(pai, filhos)

def individual_code(individual):
  code = ''
  for x in individual:
    x_bin = bin(x)
    if(x_bin[0] == '-'):
      code = code + '-' + bin(x)[3:].zfill(16)
    else:
      code = code + '+' + bin(x)[2:].zfill(16)
  return code

def code_individual(code, upperBound, lowerBound):
  individual = []
  dimension = (int)(len(code)/17)

  for i in range(dimension):
    if(code[i*17] == '+'):
      x = int('0b' + code[(i*17)+1:(i*17)+17], 2)
    elif (code[i*17] == '-'):
      x = int('-0b' + code[(i*17)+1:(i*17)+17], 2)
    if(x > (upperBound[i]*100)):
      x = upperBound[i]*100
    if(x < (lowerBound[i]*100)):
      x = lowerBound[i]*100
    individual.append(x)

  return individual

def crossover(parents, crossoverChance, mutationProb, upperBound, lowerBound, typeCrossover):
  offsprings = []

  if typeCrossover == 1:
    for pair in parents:
      if (random.random() <= crossoverChance):
        code_parent1 = individual_code(pair[0])
        code_parent2 = individual_code(pair[1])

        n_bits = len(code_parent1)
        breakpoint = random.randint(1, n_bits//2)
        #breakpoint2 = random.randint(breakpoint1, n_bits)

        code_offspring = code_parent1[:breakpoint] + code_parent2[breakpoint:]
        offspring_mutation = mutation(code_offspring, mutationProb)

        offspring = code_individual(offspring_mutation, upperBound, lowerBound)

        offsprings.append(offspring)
  elif typeCrossover == 2:
    for pair in parents:
      if (random.random() <= crossoverChance):
        code_parent1 = individual_code(pair[0])
        code_parent2 = individual_code(pair[1])

        n_bits = len(code_parent1)
        breakpoint1 = random.randint(1, n_bits-3)
        breakpoint2 = random.randint(breakpoint1, n_bits-2)

        code_offspring = code_parent1[:breakpoint1] + code_parent2[breakpoint1:breakpoint2] + code_parent1[breakpoint2:]
        offspring_mutation = mutation(code_offspring, mutationProb)

        offspring = code_individual(offspring_mutation, upperBound, lowerBound)

        offsprings.append(offspring)
  
  return offsprings

def mutation(code, mutationProb):
  offspring = ''
  for bit in code:
    if random.random() <= mutationProb:
      if bit == '0':
        offspring += '1'
      elif bit == '1':
        offspring += '0'
      else :
        offspring += bit
    else:
      offspring += bit
  return offspring

def GA(upperBound, lowerBound, max_iterations=100, crossoverProb=0.85, mutationProb=0.01, populationSize=50, fitnessFunction="Sphere", selectionType="Elitist", typeCrossover=2):
    # gerar populacao inicial
    bests = []
    population = initialPopulation(populationSize, upperBound, lowerBound)
    current_iteration = 0
    while(current_iteration < max_iterations):
        # avaliar o fitness
        populationFitness = avalFitness(population, fitnessFunction)
        # selecao dos pais
        # parents =  <<parent1,parent2>, <parent1,parent2>>
        parents = parents_selection(
            population, populationFitness, populationSize)
        # cruzamento
        offspring = crossover(parents, crossoverProb,
                              mutationProb, upperBound, lowerBound, typeCrossover)
        offspringFitness = avalFitness(offspring, fitnessFunction)
        # selecao da proxima geracao
        population = population_selection(
            population, offspring, populationFitness, offspringFitness, populationSize)
        populationFitness = avalFitness(population, fitnessFunction)

        best_individual = log(population, populationFitness, current_iteration)
        current_iteration += 1
        bests.append(best_individual[1])
    return bests

upperBound = []
lowerBound = []
for i in range(3):
  upperBound.append(5)
  lowerBound.append(-5)

tpCrossover = 1

sphere_data = []
rastringin_data = []
ackley_data = []
# CROSSOVER TYPE
while tpCrossover <= 2:
    data1 = [[] for k in range(100)]
    data2 = [[] for k in range(100)]
    data3 = [[] for k in range(100)]
    for i in range(30):
        best_individual = GA(upperBound, lowerBound,
                             fitnessFunction="Sphere", typeCrossover=tpCrossover)
        for j in range(100):
            data1[j].append(best_individual[j])
        best_individual = GA(upperBound, lowerBound,
                             fitnessFunction="Rastrigin", typeCrossover=tpCrossover)
        for j in range(100):
            data2[j].append(best_individual[j])
        best_individual = GA(upperBound, lowerBound,
                             fitnessFunction="Ackley", typeCrossover=tpCrossover)
        for j in range(100):
            data3[j].append(best_individual[j])
    print("Tipo de cruzamento: ", tpCrossover, "\n")
    sphere_data.append(data1)
    rastringin_data.append(data2)
    ackley_data.append(data3)
    tpCrossover += 1

# GRÁFICO
mean_sphere = []
error_sphere = []
i = 0
for item in sphere_data:
    mean_sphere.append([])
    error_sphere.append([])
    for data_set in item:
        mean_sphere[i].append(np.mean(data_set))
        error_sphere[i].append(np.std(data_set))
    i += 1

mean_rastrigin = []
error_rastrigin = []
i = 0
for item in rastringin_data:
    mean_rastrigin.append([])
    error_rastrigin.append([])
    for data_set in item:
        mean_rastrigin[i].append(np.mean(data_set))
        error_rastrigin[i].append(np.std(data_set))
    i += 1

mean_ackley = []
error_ackley = []
i = 0
for item in ackley_data:
    mean_ackley.append([])
    error_ackley.append([])
    for data_set in item:
        mean_ackley[i].append(np.mean(data_set))
        error_ackley[i].append(np.std(data_set))
    i += 1

x = [i for i in range(1,101)]

plt.figure()

plt.subplot(3, 1, 1)
plt.errorbar(x, mean_sphere[0], yerr=error_sphere[0])
plt.errorbar(x, mean_sphere[1], yerr=error_sphere[1])
plt.title("Sphere")
plt.legend(['Crossover tipo 1', 'Crossover tipo 2'], loc=2)
plt.xlabel("Iteração")
plt.ylabel("Fitness")

plt.subplot(3, 1, 2)
plt.errorbar(x, mean_rastrigin[0], yerr=error_rastrigin[0])
plt.errorbar(x, mean_rastrigin[1], yerr=error_rastrigin[1])
plt.title("Rastigin")
plt.legend(['Crossover tipo 1', 'Crossover tipo 2'], loc=2)
plt.xlabel("Iteração")
plt.ylabel("Fitness")

plt.subplot(3, 1, 3)
plt.errorbar(x, mean_ackley[0], yerr=error_ackley[0])
plt.errorbar(x, mean_ackley[1], yerr=error_ackley[1])
plt.title("Ackley")
plt.legend(['Crossover tipo 1', 'Crossover tipo 2'], loc=2)
plt.xlabel("Iteração")
plt.ylabel("Fitness")

plt.tight_layout()
plt.show()